﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MyListLib;

namespace MyListLib
{
    public class MyList<T> : IList<T>
    {
        private T[] Members;

        private int MembersCount;

        public MyList()
        {
            Members = new T[4];
            MembersCount = 0;
        }

        public T this[int index] 
        {
            get
            {
                if (0 <= index && index < MembersCount)
                {
                    return Members[index];
                }

                throw new ArgumentOutOfRangeException();
            }
            set
            {
                if (0 <= index && index < MembersCount)
                {
                    Members[index] = value;
                    return;
                }

                throw new ArgumentOutOfRangeException();
            }
        }

        public bool IsReadOnly => false;

        public int Count
        {
            get
            {
                return MembersCount;
            }
        }

        public void Add(T item)
        {
            if(MembersCount == Members.Length)
            {
                var newArray = new T[2 * Members.Length];

                for (int i = 0; i < MembersCount; i++)
                {
                    newArray[i] = Members[i];
                }

                Members = newArray;
            }

            Members[MembersCount] = item;
            MembersCount++;
        }

        public void Clear()
        {
            MembersCount = 0;
        }

        public bool Contains(T item)
        {
            foreach (var member in Members)
            {
                if (member.Equals(item))
                    return true;
            }

            return false;
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            if(array == null)
            {
                throw new ArgumentNullException();
            }

            if(array.Length - arrayIndex < MembersCount)
            {
                throw new ArgumentOutOfRangeException();
            }

            for(int i = 0; i < MembersCount; i++)
            {
                array[arrayIndex + i] = Members[i];
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            return new MyListEnum<T>(this);
        }

        public int IndexOf(T item)
        {
            for(int i = 0; i < MembersCount; i++)
            {
                if (item.Equals(Members[i]))
                {
                    return i;
                }
            }
            
            return -1;
        }

        public void Insert(int index, T item)
        {
            if (MembersCount < index)
                throw new ArgumentOutOfRangeException();

            // Возможность добавлять в конец
            if(MembersCount == index)
            {
                Add(item);
                return;
            }

            T buffer_item = item;

            int i = index;
            while (true)
            {
                Swap(ref Members[i], ref buffer_item);
                i++;

                if (i == MembersCount)
                {
                    Add(buffer_item);
                    break;
                }
            }
        }

        private void Swap(ref T first, ref T second)
        {
            T swap_buffer;

            swap_buffer = first;
            first = second;
            second = swap_buffer;
        }

        public bool Remove(T item)
        {
            var itemIndex = IndexOf(item);

            if(itemIndex == -1)
            {
                return false;
            }

            for (int i = itemIndex; i < MembersCount - 1; i++)
            {
                Swap(ref Members[i], ref Members[i + 1]);
            }

            MembersCount--;

            return true;
        }

        public void RemoveAt(int index)
        {
            if (!(0 <= index && index < MembersCount))
                throw new ArgumentOutOfRangeException();

            for (int i = index; i < MembersCount - 1; i++)
            {
                Swap(ref Members[i], ref Members[i + 1]);
            }

            MembersCount--;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return (IEnumerator)GetEnumerator();
        }
    }
}
