﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyListLib
{
    public class MyListEnum<T> : IEnumerator<T>
    {
        private MyList<T> MyList;

        private int Position;

        public MyListEnum(MyList<T> myList)
        {
            MyList = myList;
            Position = -1;
        }

        public T Current => MyList[Position];

        object IEnumerator.Current => Current;

        public void Dispose()
        {
            
        }

        public bool MoveNext()
        {
            if (Position + 1 == MyList.Count)
                return false;

            Position++;
            return true;
        }

        public void Reset()
        {
            Position = -1;
        }
    }
}
