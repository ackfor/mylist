﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

using MyListLib;

namespace Testing
{
    [TestClass]
    public class UnitTests
    {
        MyList<int> list;

        public UnitTests()
        {
            list = new MyList<int>();
            list.Add(0);
            list.Add(1);
            list.Add(2);
            list.Add(3);
        }

        [TestMethod]
        public void Get()
        {
            Assert.AreEqual(2, list[2]);
        }

        [TestMethod]
        public void ExeptionGet()
        {
            try
            {
                var tmp = list[-1];
                Assert.Fail();
            }
            catch (Exception e)
            {
                Assert.IsTrue(e is ArgumentOutOfRangeException);
            }

            try
            {
                var tmp = list[list.Count];
                Assert.Fail();
            }
            catch (Exception e)
            {
                Assert.IsTrue(e is ArgumentOutOfRangeException);
            }
        }

        [TestMethod]
        public void Set()
        {
            list[2] = 5;

            Assert.AreEqual(5, list[2]);
        }

        [TestMethod]
        public void ExeptionSet()
        {
            try
            {
                list[-1] = 5;
                Assert.Fail();
            }
            catch (Exception e)
            {
                Assert.IsTrue(e is ArgumentOutOfRangeException);
            }

            try
            {
                list[list.Count] = 5;
                Assert.Fail();
            }
            catch (Exception e)
            {
                Assert.IsTrue(e is ArgumentOutOfRangeException);
            }
        }

        [TestMethod]
        public void Count()
        {
            Assert.AreEqual(4, list.Count);
        }

        [TestMethod]
        public void Contains()
        {
            Assert.AreEqual(true, list.Contains(2));
        }

        [TestMethod]
        public void ContainsFalse()
        {
            Assert.AreEqual(false, list.Contains(12));
        }

        [TestMethod]
        public void CopyTo()
        {
            var array = new int[4];

            list.CopyTo(array, 0);

            for (int i = 0; i < 4; i++)
            {
                Assert.AreEqual(list[i], array[i]);
            }
        }

        [TestMethod]
        public void NullCopyTo()
        {
            try
            {
                list.CopyTo(null, 0);
            }
            catch (Exception e)
            {
                Assert.IsTrue(e is ArgumentNullException);
            }
        }

        [TestMethod]
        public void OutOfRangeCopyTo()
        {
            try
            {
                var array = new int[4];

                list.CopyTo(array, 1);
            }
            catch (Exception e)
            {
                Assert.IsTrue(e is ArgumentOutOfRangeException);
            }

            try
            {
                var array = new int[3];

                list.CopyTo(array, 0);
            }
            catch (Exception e)
            {
                Assert.IsTrue(e is ArgumentOutOfRangeException);
            }
        }

        [TestMethod]
        public void IndexOf()
        {
            Assert.AreEqual(2, list.IndexOf(2));

            Assert.AreEqual(-1, list.IndexOf(12));
        }

        [TestMethod]
        public void Insert()
        {
            list.Insert(2, 5);
            Assert.AreEqual(0, list[0]);
            Assert.AreEqual(1, list[1]);
            Assert.AreEqual(5, list[2]);
            Assert.AreEqual(2, list[3]);
            Assert.AreEqual(3, list[4]);
        }

        [TestMethod]
        public void ToEndInsert()
        {
            list.Insert(4, 5);
            Assert.AreEqual(0, list[0]);
            Assert.AreEqual(1, list[1]);
            Assert.AreEqual(2, list[2]);
            Assert.AreEqual(3, list[3]);
            Assert.AreEqual(5, list[4]);
        }

        [TestMethod]
        public void ExeptionInsert()
        {
            try
            {
                list.Insert(5, 5);
            }
            catch (Exception e)
            {
                Assert.IsTrue(e is ArgumentOutOfRangeException);
            }
        }

        [TestMethod]
        public void Remove()
        {
            list.Remove(2);
            Assert.AreEqual(3, list.Count);

            Assert.AreEqual(0, list[0]);
            Assert.AreEqual(1, list[1]);
            Assert.AreEqual(3, list[2]);
        }

        [TestMethod]
        public void RemoveEnd()
        {
            list.Remove(3);
            Assert.AreEqual(3, list.Count);

            Assert.AreEqual(0, list[0]);
            Assert.AreEqual(1, list[1]);
            Assert.AreEqual(2, list[2]);
        }

        [TestMethod]
        public void NotRemove()
        {
            list.Remove(5);

            for(int i = 0; i < list.Count; i++)
            {
                Assert.AreEqual(i, list[i]);
            }
        }

        [TestMethod]
        public void RemoveAt()
        {
            list.RemoveAt(1);

            Assert.AreEqual(3, list.Count);

            Assert.AreEqual(0, list[0]);
            Assert.AreEqual(2, list[1]);
            Assert.AreEqual(3, list[2]);
        }

        [TestMethod]
        public void RemoveAtEnd()
        {
            list.RemoveAt(list.Count-1);

            Assert.AreEqual(3, list.Count);

            Assert.AreEqual(0, list[0]);
            Assert.AreEqual(1, list[1]);
            Assert.AreEqual(2, list[2]);
        }

        [TestMethod]
        public void ExeptionRemoveAt()
        {
            try
            {
                list.RemoveAt(-1);
            }
            catch (Exception e)
            {
                Assert.IsTrue(e is ArgumentOutOfRangeException);
            }

            try
            {
                list.RemoveAt(list.Count);
            }
            catch (Exception e)
            {
                Assert.IsTrue(e is ArgumentOutOfRangeException);
            }
        }
    }
}
